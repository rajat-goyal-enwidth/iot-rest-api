# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###

1. download the elasticsearch version 7.15.0 if not downloaded


2. run the elasticsearch first 

	1. go the downloaded elasticsearch, open the folder **elasticsearch 7.15.0**
	2. go to bin folder and open it in cmd and execute, **elasticsearch.bat**
	

3. paste all the properties in application.properties of the spring boot application

```application.properties
#----------------properties to configure JWT------------------------------

	jwt.secret = enwidth
	jwt.expirationDateInMs=18000000

#----------------properties to elastic search------------------------------

	elasticsearch.url=localhost:9200

#----------------properties to configure mysql------------------------------

	spring.datasource.url=jdbc:mysql://localhost:3306/iot?createDatabaseIfNotExist=true&useSSL=false
	spring.datasource.username=root
	spring.datasource.password=root
	spring.jpa.hibernate.ddl-auto=update


#--------------------application port ---------------------------------
	server.port=8080

```	


### How to use elastic search or mysql database ###

1. **To use ElasticSearch in the project, please follow these steps**
	1.	Open **AuthenticationController** from package **com.root.controller**   and paste the given below and import the packages  
		```AuthenticationController
			@Autowired
			private ElasticCustomUserDetailsService userDetailsService;
		```
	2.	Open **AdminController** from package **com.root.controller** and paste the given below and import the packages
		```AdminController
	
			@Autowired
			AdminElasticService adminService;
	
			@Autowired
			IOTDeviceElasticService iotDeviceService;
		```
	3. 	Open **UserController** from package **com.root.controller** and paste the given below and import the packages
		```UserController
			@Autowired
			DeviceRegisterElasticService deviceRegisterService;
	
			@Autowired
			IOTDeviceElasticService iotDeviceService;
		```


2. **To use MYSQL in the project, please follow these steps**
	1.	Open **AuthenticationController** from package **com.root.controller**   and paste the given below and import the packages  
		```AuthenticationController
			@Autowired
			JpaCustomUserDetailsService userDetailsService;
		```
	2.	Open **AdminController** from package **com.root.controller** and paste the given below and import the packages
		```AdminController
	
			@Autowired
			AdminJpaService adminService;

			@Autowired
			IOTDeviceJpaService iotDeviceService;
		```
	3. 	Open **UserController** from package **com.root.controller** and paste the given below and import the packages
		```UserController
			@Autowired
			DeviceRegisterJpaService deviceRegisterService;
	
			@Autowired
			IOTDeviceJpaService iotDeviceService;
		```
		


4. run the project as spring boot app



### How do I open all the elastic search documents ###

[URL to check elasticsearch](http://localhost:9200)

[URL to view all the documents created on elasticsearch](http://localhost:9200/_cat/indices)

[URL to view all the iot_user details](http://localhost:9200/iot_users/_search?pretty=true&q=*:*)
	
[URL to view all the iot_devices details](http://localhost:9200/iot_devices/_search?pretty=true&q=*:*)

[URL to view all the device_register details](http://localhost:9200/device_register/_search?pretty=true&q=*:*)

		

### To open all the api url ###

import the IOT.postman_collection json file in postman