package com.root.iot.config.mqtt;

import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.mqtt.core.DefaultMqttPahoClientFactory;
import org.springframework.integration.mqtt.core.MqttPahoClientFactory;
import org.springframework.integration.mqtt.inbound.MqttPahoMessageDrivenChannelAdapter;
import org.springframework.integration.mqtt.support.DefaultPahoMessageConverter;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;

import com.root.iot.payload.service.PayloadType1Service;

@Configuration
public class MqttInBoundConfiguration {

	
	@Autowired
	PayloadType1Service payloadService;

	
	public MqttPahoClientFactory mqttClientFactory() {

		DefaultMqttPahoClientFactory factory = new DefaultMqttPahoClientFactory();
		MqttConnectOptions options = new MqttConnectOptions();

		options.setServerURIs(new String[] { "tcp://52.45.136.134:1883" });
		options.setCleanSession(true);

		factory.setConnectionOptions(options);
		return factory;
	}

	
	@Bean
	public MessageChannel mqttInputChannel() {
		return new DirectChannel();
	}

	
	@Bean
	public MessageProducer inbound() {

		MqttPahoMessageDrivenChannelAdapter adapter = new MqttPahoMessageDrivenChannelAdapter("serverIn",
				mqttClientFactory(), "demoTopic2017");

		//adding topic "register" on which mobile device will send the MacAddress
		//adapter.addTopic("register");
		
		adapter.setCompletionTimeout(5000);
		adapter.setConverter(new DefaultPahoMessageConverter());
		adapter.setQos(2);
		adapter.setOutputChannel(mqttInputChannel());
		return adapter;
		
	}

	
	@Bean
	@ServiceActivator(inputChannel = "mqttInputChannel")
	public MessageHandler handler() {
 
		return message -> {
						
			String topic = message.getHeaders().get("mqtt_receivedTopic").toString();
			String payload = message.getPayload().toString();

			if (topic.equals("demoTopic2017") || topic.equals("register")) {
				System.out.println("this is our topic : " + topic);
				System.out.println("PAYLOAD : " + payload);
			} else {
				System.out.println(topic + " : " + payload);
			} 
		};
	}

} 
