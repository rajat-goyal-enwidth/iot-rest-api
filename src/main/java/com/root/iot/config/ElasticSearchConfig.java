package com.root.iot.config;

import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.root.iot.repository.elastic")
@EnableJpaRepositories(basePackages = {"com.root.iot.repository.jpa","com.root.iot.audit.repository"})

@ComponentScan(basePackages = {"com.root"})
public class ElasticSearchConfig extends AbstractElasticsearchConfiguration{

	
	    @Value("${elasticsearch.url}")
	    public String elasticsearchUrl;

	    @Bean
	    @Override
	    public RestHighLevelClient elasticsearchClient() {
	        final ClientConfiguration config = ClientConfiguration.builder()
	                .connectedTo(elasticsearchUrl)
	                .build();

	        return RestClients.create(config).rest();
	    }
}
