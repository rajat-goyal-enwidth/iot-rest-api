package com.root.iot.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.root.iot.audit.service.UserAuditService;
import com.root.iot.model.DAOUser;
import com.root.iot.model.UserDTO;
import com.root.iot.repository.jpa.UserJpaRepository;

@Service
public class CustomUserDetailsService implements UserDetailsService {

	
	@Autowired
	private UserJpaRepository userDao;
	
	@Autowired
	private PasswordEncoder bcryptEncoder;
	
	@Autowired
	UserAuditService userAuditService;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		List<SimpleGrantedAuthority> roles = null;
				
		//fetching user credentials from elastic search
		DAOUser user = userDao.findByUsername(username);
		
		//validating user based on roles using elastic search		
		if (user != null) {
			roles = Arrays.asList(new SimpleGrantedAuthority(user.getRole()));
			
			return new User(user.getUsername(), user.getPassword(), roles);
		}
		
		throw new UsernameNotFoundException("User not found with the name " + username);	
		
	}
	
	public DAOUser save(UserDTO user) {
		DAOUser newUser = new DAOUser();
		newUser.setUsername(user.getUsername());
		newUser.setEmail(user.getEmail());
		newUser.setPassword(bcryptEncoder.encode(user.getPassword()));
		newUser.setRole(user.getRole());
		
//		String action = newUser.getUsername()+" saved successfully ";
//		userAuditService.saveUserAuditDetails(action);
		
		//saving user on elastic search
		userDao.save(newUser);
			
		return newUser;
	}

}
