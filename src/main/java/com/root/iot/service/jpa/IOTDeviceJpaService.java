package com.root.iot.service.jpa;
 
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.service.IOTDeviceAuditService;
import com.root.iot.exception.IOTDeviceException;
import com.root.iot.exception.IOTDeviceNotFoundByMacAddress;
import com.root.iot.exception.IOTDeviceNotFoundByUserId;
import com.root.iot.model.DAOUser;
import com.root.iot.model.IOTDevice;
import com.root.iot.repository.jpa.IOTDeviceJpaRepository;

@Service
public class IOTDeviceJpaService {

	@Autowired
	IOTDeviceAuditService auditService;
	
	@Autowired
	IOTDeviceJpaRepository iotDeviceRepo;
	
	@Autowired
	UserJpaService userService;
	
	
	
	//this method used to add IOT Device 
	public IOTDevice saveIOTDevice(IOTDevice iotDevice) throws IOTDeviceException
	{
		//fetching current user Details to save user id with IOTDevice
		DAOUser user = userService.currentUserDetails();
				
		boolean check = checkDeviceIsAlreadyExistWithMacAddress(iotDevice.getDeviceMacAddress());
				
		//if device with same MacAddress already added then we will not add that device again 
		if(check) {
			throw new IOTDeviceException("IOT Device already added with MacAddress "+iotDevice.getDeviceMacAddress());
		}
		else
		{
			IOTDevice device = new IOTDevice();
			device.setUserId(user.getId());
			device.setDeviceName(iotDevice.getDeviceName());
			device.setDeviceMacAddress(iotDevice.getDeviceMacAddress());
			
			IOTDevice iot = iotDeviceRepo.save(device);
			
			//used to audit, when the iot device added in the db audit info will saved
			String action = "Iot device added";
			auditService.saveDeviceAudit(iot.getDeviceId(), action);
			
			return device;
		}
		
		
	}	
	
		 
	
	//this method used to get deviceId by macAddress
	public String findDeviceID_ByMacAddress(String macAddress) throws IOTDeviceNotFoundByMacAddress
	{
		IOTDevice device = iotDeviceRepo.findBydeviceMacAddress(macAddress);
		
		if(device == null) {
			throw new IOTDeviceNotFoundByMacAddress("Sorry, IOT Device is not added with MacAddress "+macAddress+"\nPlease add your device first");
		}
		
		return device.getDeviceId();
	}
	
	
	//method to get all the IOTDevices added by particular user
	public List<IOTDevice> findAllIotDevicesAddedByUser(String userId) throws IOTDeviceNotFoundByUserId
	{
		List<IOTDevice> devices = iotDeviceRepo.findAllByuserId(userId);
		
		if(devices.isEmpty()) {
			throw new IOTDeviceNotFoundByUserId("Sorry, No Device has been added by given userId "+userId);			
		}
		
		return devices;
	}
		
	
	//method to check whether device already exist or not, if not then we will add device
	public boolean checkDeviceIsAlreadyExistWithMacAddress(String macAddress)
	{
		IOTDevice device = iotDeviceRepo.findBydeviceMacAddress(macAddress);
		
		if(device != null) {
			return true;
		}
		else {
			return false;
		}
	}

		
	
	//this method used to get the IOTDevice by using devideID
	public IOTDevice findIOTDeviceByID(String id) throws IOTDeviceException
	{
		Optional<IOTDevice> iotDevice = iotDeviceRepo.findById(id);		
		if(iotDevice == null){
			throw new IOTDeviceException("IOT Device "+id+" is not available");
		}
		
		IOTDevice iotDevicenew = new IOTDevice(iotDevice.get().getUserId(), iotDevice.get().getDeviceName(),
				iotDevice.get().getDeviceMacAddress());
		
		return iotDevicenew;
	}
	
	
	
	//method to delete IOTDevice	
	public IOTDevice deleteIOTDevice(String id)
	{
		IOTDevice iotDevice = findIOTDeviceByID(id);	
		
		iotDeviceRepo.deleteById(id);		
		
		//used to audit, when the iot device deleted from the db audit info will saved
		String action = "Iot device deleted";
		auditService.saveDeviceAudit(iotDevice.getDeviceId(), action);
		return iotDevice;
	}
	

}
