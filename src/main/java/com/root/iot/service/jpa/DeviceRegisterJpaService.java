package com.root.iot.service.jpa;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.service.DeviceRegisterAuditService;
import com.root.iot.exception.DeviceRegisterException;
import com.root.iot.exception.RegisteredDeviceNotFoundByMacAddress;
import com.root.iot.exception.RegisteredDeviceNotFoundByOwnerName;
import com.root.iot.exception.RegisteredDeviceNotFoundByUserId;
import com.root.iot.model.DAOUser;
import com.root.iot.model.DeviceRegister;
import com.root.iot.repository.jpa.DeviceRegisterJpaRepository;

@Service
public class DeviceRegisterJpaService {

	@Autowired
	UserJpaService userService;
	
	@Autowired
	DeviceRegisterJpaRepository deviceRegisterRepo;
	
	@Autowired
	IOTDeviceJpaService iotdeviceService;
	
	@Autowired
	DeviceRegisterAuditService auditService;
	
	
	
	public DeviceRegister saveDeviceRegister(String ownerName, String ownerPhoneNo, String macAddress) throws DeviceRegisterException
	{ 
		//used to get current user details to save user Id during device registration
		DAOUser currentUserDetails = userService.currentUserDetails();
		
		//user to check whether the iot device is added or not , if not then user needs to add the iot device first
		iotdeviceService.findDeviceID_ByMacAddress(macAddress);
		
		//used to get device  by using macAddress to check whether device is already registered or not		
		DeviceRegister registeredDevice = deviceRegisterRepo.findBydeviceMacAddress(macAddress);
		
		if(registeredDevice == null)	{
			
			DeviceRegister device = new DeviceRegister();		
			device.setUserId(currentUserDetails.getId());
			device.setDeviceMacAddress(macAddress);
			device.setOwnerName(ownerName);
			device.setOwnerPhoneNo(ownerPhoneNo);
			
			DeviceRegister registerDevice = deviceRegisterRepo.save(device);
			
			//used to audit, when the iot device registered in the db audit info will saved
			String action = "device registered";
			auditService.deviceRegisterAudit(registerDevice.getId(), action);
			
			return registerDevice;
			
		}		
		else	{			
			
			throw new DeviceRegisterException("The device with macAddress "+macAddress+" already registered");
		}
				
	}
	
	

	public DeviceRegister findRegisteredDeviceByMacAddress(String macaddress) throws RegisteredDeviceNotFoundByMacAddress
	{
		DeviceRegister device = deviceRegisterRepo.findBydeviceMacAddress(macaddress);
		
		if(device==null) {
			throw new RegisteredDeviceNotFoundByMacAddress("Sorry, Device is not registered with "+macaddress+"");
		}
		
		 return device;
	}
	
	
	
	
	public List<DeviceRegister> findAllRegisteredDeviceByUserID(String userId) throws RegisteredDeviceNotFoundByUserId
	{
		List<DeviceRegister> devices = deviceRegisterRepo.findAllByuserId(userId);
		
		if(devices.isEmpty()) {
			throw new RegisteredDeviceNotFoundByUserId("Sorry, you have not registered any device ");
		}
		
		return devices;
	}
	
	
	
	
	public DeviceRegister findRegisteredDeviceByOwnerName(String ownerName) throws RegisteredDeviceNotFoundByOwnerName
	{
		DeviceRegister device = deviceRegisterRepo.findByownerName(ownerName);
		if(device == null) {
			throw new RegisteredDeviceNotFoundByOwnerName("Sorry, device is not registered with given name "+ownerName);
		}
		return device;
	}
	
	
	
	
	//method used to update owner name and owner phone no
	public DeviceRegister updateOwnerNameAndOwnerPhoneNo(String newownername, 
			String newownerphoneno, String registeredMacAddress) throws RegisteredDeviceNotFoundByMacAddress
	{
		//used to get registeredDevice by using device MacAddress if exist
		DeviceRegister registeredDevice = deviceRegisterRepo.findBydeviceMacAddress(registeredMacAddress);
		
		if(registeredDevice != null) {
			
			registeredDevice.setId(registeredDevice.getId());
			registeredDevice.setUserId(registeredDevice.getUserId());
			registeredDevice.setDeviceMacAddress(registeredDevice.getDeviceMacAddress());
			registeredDevice.setOwnerName(newownername);
			registeredDevice.setOwnerPhoneNo(newownerphoneno);
			
			//used to audit, when the iot registered device's details will be updated in the db audit info will be saved
			String action = "registered device details updated";
			auditService.deviceRegisterAudit(registeredDevice.getId(), action);
			
			return deviceRegisterRepo.save(registeredDevice);
			
		}
		else	{
			
			throw new RegisteredDeviceNotFoundByMacAddress("Sorry, you have not registered any device with Mac Address"+registeredMacAddress+".\nPlease registered device first to update the details");
		}
	}
	
	
	
	public String deleteRegisteredDevice(String id)
	{
		boolean device = deviceRegisterRepo.existsById(id);
		
		if(device) {
			
			//used to audit, when theregistered device will be deleted from the db audit info will saved
			String action = "registered device deleted";
			auditService.deviceRegisterAudit(id, action);
			deviceRegisterRepo.deleteById(id);
			
			return action;		
		}
		
		else {
			
			return "Device is not registered with given id";
		}
	}
	
	
}
