package com.root.iot.service.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.root.iot.model.DAOUser;
import com.root.iot.repository.jpa.UserJpaRepository;

@Service
public class AdminJpaService {

	@Autowired
	UserJpaRepository userRepo;
	
	public DAOUser findUser(String username) throws UsernameNotFoundException
	{	
		DAOUser user = userRepo.findByUsername(username);
		if(user != null)
		{
			return user;
		}
		
		throw new UsernameNotFoundException("User not found "+username);		
	}
	
	
	public DAOUser deleteUser(String username) throws UsernameNotFoundException
	{
		DAOUser user = userRepo.findByUsername(username);
		if( user != null)
		{
			userRepo.deleteById(user.getId());
			return user;
		}
		
		throw new UsernameNotFoundException("User not found "+username);	
	}
}
