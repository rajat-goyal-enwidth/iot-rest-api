package com.root.iot.service.jpa;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.root.iot.model.DAOUser;
import com.root.iot.repository.jpa.UserJpaRepository;

@Service
public class UserJpaService {

	@Autowired
	UserJpaRepository userRepo;

	public DAOUser currentUserDetails() {
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String username = userDetails.getUsername();

		DAOUser user = userRepo.findByUsername(username);

		return user;
	}

}
