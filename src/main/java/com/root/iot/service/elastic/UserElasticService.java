package com.root.iot.service.elastic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import com.root.iot.model.DAOUser;
import com.root.iot.repository.elastic.UserElasticRepository;

@Service
public class UserElasticService {

	@Autowired
	UserElasticRepository userRepo;

	public DAOUser currentUserDetails() {
		
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

		String username = userDetails.getUsername();

		DAOUser user = userRepo.findByUsername(username);

		return user;
	}

}
