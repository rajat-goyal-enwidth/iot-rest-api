package com.root.iot.service.elastic;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.IOTDeviceAudit;
import com.root.iot.repository.elastic.IOTDeviceAuditElasticRepository;

@Service
public class IOTDeviceAuditElasticService {

	@Autowired
	IOTDeviceAuditElasticRepository auditRepo;
	
	public void saveDeviceAudit(String deviceId, String action)
	{	   
		 IOTDeviceAudit auditdevice = new IOTDeviceAudit();		 
		 auditdevice.setDeviceId(deviceId);
		 auditdevice.setAction(action);		 
		 System.out.println("device audit details "+auditdevice);	 
		 auditRepo.save(auditdevice);
	}	
	
	
}
