package com.root.iot.service.elastic;


import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.exception.DeviceRegisterException;
import com.root.iot.exception.RegisteredDeviceNotFoundByMacAddress;
import com.root.iot.exception.RegisteredDeviceNotFoundByOwnerName;
import com.root.iot.exception.RegisteredDeviceNotFoundByUserId;
import com.root.iot.model.DAOUser;
import com.root.iot.model.DeviceRegister;
import com.root.iot.repository.elastic.DeviceRegisterElasticRepository;

@Service
public class DeviceRegisterElasticService {

	@Autowired
	UserElasticService userService;
	
	@Autowired
	IOTDeviceElasticService iotdeviceService;
	
	@Autowired
	DeviceRegisterElasticRepository deviceRegisterRepo;	
	
	public DeviceRegister saveDeviceRegister(String ownerName, String ownerPhoneNo, String macAddress) throws DeviceRegisterException
	{
		//used to get current user details to save user Id during device registration
		DAOUser currentUserDetails = userService.currentUserDetails();
		
		//user to check whether the iot device is added or not , if not then user needs to add the iot device first
		iotdeviceService.findDeviceID_ByMacAddress(macAddress);
				
		//used to get the registered device by using macAddress to check whether the device	is already registered or not
		DeviceRegister registeredDevice = deviceRegisterRepo.findBydeviceMacAddress(macAddress);
		
		if(registeredDevice == null)
		{
			DeviceRegister device = new DeviceRegister();		
			device.setUserId(currentUserDetails.getId());
			device.setDeviceMacAddress(macAddress);
			device.setOwnerName(ownerName);
			device.setOwnerPhoneNo(ownerPhoneNo);
			return deviceRegisterRepo.save(device);
		}		
		else {			
			throw new DeviceRegisterException("The device with macAddress "+macAddress+" already registered");
		}
				
	}

	
	
	public List<DeviceRegister> findAllRegisteredDeviceByUserID(String userId) throws RegisteredDeviceNotFoundByUserId
	{
		List<DeviceRegister> devices = deviceRegisterRepo.findAllByuserId(userId);
		
		if(devices.isEmpty()) {
			throw new RegisteredDeviceNotFoundByUserId("Sorry, you have not registered any device ");
		}
		
		return devices;
	}
	
	
	
	public DeviceRegister findRegisteredDeviceByOwnerName(String ownerName) throws RegisteredDeviceNotFoundByOwnerName
	{
		DeviceRegister device = deviceRegisterRepo.findByownerName(ownerName);
		if(device == null) {
			throw new RegisteredDeviceNotFoundByOwnerName("Sorry, device is not registered with given name "+ownerName);
		}
		return device;
	}
	
	
	//method used to update owner name and owner phone no
	public DeviceRegister updateOwnerNameAndOwnerPhoneNo(String newownername, 
			String newownerphoneno, String registeredMacAddress) throws RegisteredDeviceNotFoundByMacAddress
	{
		//used to get registeredDevice by using device id if exist
		DeviceRegister registeredDevice = deviceRegisterRepo.findBydeviceMacAddress(registeredMacAddress);
		
		if(registeredDevice != null) {
			
			registeredDevice.setId(registeredDevice.getId());
			registeredDevice.setUserId(registeredDevice.getUserId());
			registeredDevice.setDeviceMacAddress(registeredDevice.getDeviceMacAddress());
			registeredDevice.setOwnerName(newownername);
			registeredDevice.setOwnerPhoneNo(newownerphoneno);
			
			return deviceRegisterRepo.save(registeredDevice);
			
		}
		else
		{
			throw new RegisteredDeviceNotFoundByMacAddress("Sorry, you have not registered any device with Mac Address"+registeredMacAddress+".\nPlease registered device first to update the details");
		}
	}
	
	
	
}
