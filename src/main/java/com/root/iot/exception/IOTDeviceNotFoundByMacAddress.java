package com.root.iot.exception;

public class IOTDeviceNotFoundByMacAddress extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public IOTDeviceNotFoundByMacAddress(String str)
	{
		super(str);
	}
}
