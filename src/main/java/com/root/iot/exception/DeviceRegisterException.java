package com.root.iot.exception;

public class DeviceRegisterException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public DeviceRegisterException(String str)
	{
		super(str);
	}

}
