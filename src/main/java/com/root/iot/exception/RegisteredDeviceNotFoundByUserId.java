package com.root.iot.exception;

public class RegisteredDeviceNotFoundByUserId extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public RegisteredDeviceNotFoundByUserId(String str)
	{
		super(str);
	}
}
