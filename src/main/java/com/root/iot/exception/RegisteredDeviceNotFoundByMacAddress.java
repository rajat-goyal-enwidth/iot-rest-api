package com.root.iot.exception;

public class RegisteredDeviceNotFoundByMacAddress extends RuntimeException{

	private static final long serialVersionUID = 1L;

	public RegisteredDeviceNotFoundByMacAddress(String str)
	{
		super(str);
	}
}
