package com.root.iot.exception;

public class IOTDeviceNotFoundByUserId extends RuntimeException {


	private static final long serialVersionUID = 1L;

	public IOTDeviceNotFoundByUserId(String str)
	{
		super(str);
	}
}
