package com.root.iot.exception;

public class RegisteredDeviceNotFoundByOwnerName extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public RegisteredDeviceNotFoundByOwnerName(String str)
	{
		super(str);
	}
}
