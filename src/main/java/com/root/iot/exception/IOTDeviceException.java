package com.root.iot.exception;

public class IOTDeviceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IOTDeviceException(String str)
	{
		super(str);
	}
}
