package com.root.iot.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Entity
@Table(name = "device_register")
@Document(indexName = "device_register")
public class DeviceRegister {

	@Id
	@Field(type = FieldType.Keyword)
	@org.springframework.data.annotation.Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	String id;

	String userId;
	
	String deviceMacAddress;
	
	String ownerPhoneNo;
	
	String ownerName;
	
	
	public DeviceRegister() {
		// TODO Auto-generated constructor stub
	}

	public DeviceRegister(String id, String userId, String deviceMacAddress, String ownerPhoneNo, String ownerName) {
		super();
		this.id = id;
		this.userId = userId;
		this.deviceMacAddress = deviceMacAddress;
		this.ownerPhoneNo = ownerPhoneNo;
		this.ownerName = ownerName;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	public String getOwnerPhoneNo() {
		return ownerPhoneNo;
	}

	public void setOwnerPhoneNo(String ownerPhoneNo) {
		this.ownerPhoneNo = ownerPhoneNo;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	
	@Override
	public String toString() {
		return "DeviceRegister [id=" + id + ", userId =" +userId+", deviceMacAddress=" + deviceMacAddress + ", ownerPhoneNo=" + ownerPhoneNo + ", ownerName="
				+ ownerName + "]";
	}

	
		
}
