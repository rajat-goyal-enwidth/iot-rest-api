package com.root.iot.model;

import javax.persistence.Entity; 
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;


@Entity
@Table(name = "iot_devices")
@Document(indexName = "iot_devices")
public class IOTDevice {

	@Id
	@Field(type = FieldType.Keyword)
	@org.springframework.data.annotation.Id
	@GeneratedValue(generator="system-uuid")
	@GenericGenerator(name="system-uuid", strategy = "uuid")
	String deviceId;
	
	String userId;
	
	String deviceName;
	
	String deviceMacAddress;

	public IOTDevice() {
		
	}
	

	public IOTDevice(String userId, String deviceName, String deviceMacAddress) {
		super();
		this.userId = userId;
		this.deviceName = deviceName;
		this.deviceMacAddress = deviceMacAddress;
	}

	

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	
	
	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getDeviceMacAddress() {
		return deviceMacAddress;
	}

	public void setDeviceMacAddress(String deviceMacAddress) {
		this.deviceMacAddress = deviceMacAddress;
	}

	

	@Override
	public String toString() {
		return "IOTDevice [deviceId=" + deviceId + ", userId=" + userId + ", deviceName=" + deviceName
				+ ", deviceMacAddress=" + deviceMacAddress + "]";
	}

	
}

