package com.root.iot.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.audit.Payload2Audit;

@Repository
public interface Payload2AuditRepository extends JpaRepository<Payload2Audit, String>{

}
