package com.root.iot.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.audit.Payload1Audit;

@Repository
public interface Payload1AuditRepository extends JpaRepository<Payload1Audit, String>{

}
