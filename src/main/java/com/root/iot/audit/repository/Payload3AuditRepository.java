package com.root.iot.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.audit.Payload3Audit;

@Repository
public interface Payload3AuditRepository extends JpaRepository<Payload3Audit, String>{

}
