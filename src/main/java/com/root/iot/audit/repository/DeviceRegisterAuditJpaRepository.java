package com.root.iot.audit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.audit.DeviceRegisterAudit;

@Repository
public interface DeviceRegisterAuditJpaRepository extends JpaRepository<DeviceRegisterAudit, String>{

}
