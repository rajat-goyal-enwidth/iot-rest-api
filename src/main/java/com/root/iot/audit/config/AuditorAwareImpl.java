package com.root.iot.audit.config;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import com.root.iot.model.DAOUser;
import com.root.iot.service.jpa.UserJpaService;

public class AuditorAwareImpl implements AuditorAware<String> {

	@Autowired
	UserJpaService userService;
	
    @Override
    public Optional<String> getCurrentAuditor(){   	
    	
    	DAOUser currentUser = userService.currentUserDetails();     
    	return Optional.of(currentUser.getUsername());
    
    }
    
}