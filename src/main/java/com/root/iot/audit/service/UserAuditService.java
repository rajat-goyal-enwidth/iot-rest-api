package com.root.iot.audit.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.UserAudit;
import com.root.iot.audit.repository.UserAuditJpaRepository;

@Service
public class UserAuditService {

	@Autowired
	UserAuditJpaRepository userAuditRepo;

	public void saveUserAuditDetails(String action) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();

		UserAudit audit = new UserAudit();
		audit.setAuditTime(dtf.format(now));
		audit.setAction(action);

		userAuditRepo.save(audit);
	}

}
