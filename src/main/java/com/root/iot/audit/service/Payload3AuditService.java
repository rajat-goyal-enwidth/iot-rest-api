package com.root.iot.audit.service;

import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.Payload3Audit;
import com.root.iot.audit.repository.Payload3AuditRepository;

@Service
public class Payload3AuditService {

	
	@Autowired
	Payload3AuditRepository payload3AuditRepo;
	
	public void savePayload3AuditDetails(String macAddress, String action, String command) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		
		Payload3Audit audit = new Payload3Audit();
		audit.setMacAddress(macAddress);
		audit.setCommand(command);
		audit.setAuditTime(dtf.format(now));
		audit.setAction(action);
				
		payload3AuditRepo.save(audit);
		
	}
}
