package com.root.iot.audit.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.Payload1Audit;
import com.root.iot.audit.repository.Payload1AuditRepository;

@Service
public class Payload1AuditService {

	@Autowired
	Payload1AuditRepository payload1AuditRepo;
	
	public void savePayload1AuditDetails(String macAddress, String action) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		
		Payload1Audit audit = new Payload1Audit();
		audit.setMacAddress(macAddress);
		audit.setAuditTime(dtf.format(now));
		audit.setAction(action);
		
		payload1AuditRepo.save(audit);
		
	}
}
