package com.root.iot.audit.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.Payload2Audit;
import com.root.iot.audit.repository.Payload2AuditRepository;

@Service
public class Payload2AuditService {

	@Autowired
	Payload2AuditRepository payload2AuditRepo;
	
	public void savePayload2AuditDetails(String macAddress, String action) {
		
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		
		Payload2Audit audit = new Payload2Audit();
		audit.setMacAddress(macAddress);
		audit.setAuditTime(dtf.format(now));
		audit.setAction(action);
		
		payload2AuditRepo.save(audit);
		
	}
}
