package com.root.iot.audit.service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.DeviceRegisterAudit;
import com.root.iot.audit.repository.DeviceRegisterAuditJpaRepository;

@Service
public class DeviceRegisterAuditService {

	@Autowired
	DeviceRegisterAuditJpaRepository deviceAuditRepo;
	
	public void deviceRegisterAudit(String deviceId, String action)
	{
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		 LocalDateTime now = LocalDateTime.now();
		   		 
		 DeviceRegisterAudit auditdevice = new DeviceRegisterAudit();		 
		 auditdevice.setDeviceId(deviceId);
		 auditdevice.setAuditTime(dtf.format(now));
		 auditdevice.setAction(action);		 
		 
		 deviceAuditRepo.save(auditdevice);
		 
	}	
}
