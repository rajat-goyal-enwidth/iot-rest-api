package com.root.iot.audit.service;

import java.time.LocalDateTime; 
import java.time.format.DateTimeFormatter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.root.iot.audit.IOTDeviceAudit;
import com.root.iot.audit.repository.IOTDeviceAuditJpaRepository;

@Service
public class IOTDeviceAuditService {

	@Autowired
	IOTDeviceAuditJpaRepository auditRepo;
	
	public void saveDeviceAudit(String deviceId, String action)
	{
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		 LocalDateTime now = LocalDateTime.now();
		   		 
		 IOTDeviceAudit auditdevice = new IOTDeviceAudit();		 
		 auditdevice.setDeviceId(deviceId);
		 auditdevice.setAuditTime(dtf.format(now));
		 auditdevice.setAction(action);		 
		 
		 auditRepo.save(auditdevice);
	}	
	
	
}
