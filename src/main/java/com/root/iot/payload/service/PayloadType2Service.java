package com.root.iot.payload.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.root.iot.audit.service.Payload2AuditService;
import com.root.iot.mqtt.gateway.MqttGateway;
import com.root.iot.payload.PayloadType2;

@Service
public class PayloadType2Service {

	@Autowired
	MqttGateway mqqtGateway;
	
	@Autowired
	Payload2AuditService payloadService;
	
	
	//PayLoad CASE 2 In the case 2 : Input parameter will be {"type":2} 
	//	when it reaches to the device then device will call re register.
	
	//-> input topic will be :- MacAddress
	//-> publish message will be :- type : 2
	
	public String deviceAcknowledgementType2(String topic)
	{
		PayloadType2 payload = new PayloadType2();
		
		payload.setType("2");
		
		//publishing message on topic mac address
		mqqtGateway.sendToMqtt(topic, new Gson().toJson(payload));
		
		//auditing payload 2 publishing message
		String action = "type 2 published on mac address";
		payloadService.savePayload2AuditDetails(topic, action);
		return new Gson().toJson(payload);		
	}
	
}
