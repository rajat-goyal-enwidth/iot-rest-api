package com.root.iot.payload.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.root.iot.audit.service.Payload3AuditService;
import com.root.iot.mqtt.gateway.MqttGateway;
import com.root.iot.payload.PayloadType3;

@Service
public class PayloadType3Service {

	@Autowired
	MqttGateway mqttGateway;

	@Autowired
	Payload3AuditService payloadService;
	
	
	// In case 3 you have to publish below message on the topic MAC address
	// Case 3 : {"type":3 ,""cmd":"o","rtc":"","pin":"" } or {"type":3
	// ,""cmd":"c","rtc":"","pin":""}

	public String deviceAcknowledgementType3(String topic, String command) {
				
		PayloadType3 payload = new PayloadType3();
		
		String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

		payload.setType("3");
		payload.setCmd(command);
		payload.setRtc(timeStamp);
		payload.setPin("560063");
		
		//publishing message on topic mac address
		mqttGateway.sendToMqtt(topic, new Gson().toJson(payload));

		//auditing payload 3 published message
		String action = "user instructed : "+command+" : command";
		payloadService.savePayload3AuditDetails(topic, action, command);
		
		
		return new Gson().toJson(payload);
	}
}
