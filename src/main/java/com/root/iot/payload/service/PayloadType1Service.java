package com.root.iot.payload.service;

import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.root.iot.audit.service.Payload1AuditService;
import com.root.iot.model.DeviceRegister;
import com.root.iot.mqtt.gateway.MqttGateway;
import com.root.iot.payload.PayloadType1;
import com.root.iot.repository.jpa.DeviceRegisterJpaRepository;

@Service
public class PayloadType1Service {

	@Autowired
	DeviceRegisterJpaRepository deviceRepo;
	
	
	@Autowired
	Payload1AuditService payloadService;
	
	@Autowired
	MqttGateway mqqtGateway;
	
	
	//PayLoad CASE 1
	//In case 1 :- we will check whether the macAddress is registered or not 
	//if MacAddress is registered we will send payload :- {"type":1, "s":"s", "key":"ASHFHF$%$HF$$$EY"}
	//if MacAddress is not registered we will send payload :- {"type":1, "s":"f"}
	
	public String deviceAcknowledgementType1(String deviceMacAddress) throws JsonProcessingException {
		
		PayloadType1 payload = new PayloadType1();			
		DeviceRegister device = deviceRepo.findBydeviceMacAddress(deviceMacAddress);
		StringBuilder action = new StringBuilder();
		
		if (device != null) {
			
			payload.setType("1");
			payload.setS("s");
			payload.setKey("ASHFHF$%$HF$$$EY");		
			
			//publishing message on mac address
			mqqtGateway.sendToMqtt(deviceMacAddress, new Gson().toJson(payload));
			
			//auditing the payload 1 published message if device registered successfully
			action.append("device registration success");
			payloadService.savePayload1AuditDetails(deviceMacAddress, action.toString());
			
			return new Gson().toJson(payload);
		}

		else {				
			payload.setType("1");
			payload.setS("f");
			
			//auditing the payload 1 published message if device registered failed
			action.append("device registration failed");
			payloadService.savePayload1AuditDetails(deviceMacAddress, action.toString());
			return new Gson().toJson(payload);
		}

	}
}
