package com.root.iot.repository.elastic;
 
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.model.DAOUser;

@Repository
public interface UserElasticRepository extends ElasticsearchRepository<DAOUser, String>{

	DAOUser findByUsername(String username);
}
