package com.root.iot.repository.elastic;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.model.DeviceRegister;

@Repository
public interface DeviceRegisterElasticRepository extends ElasticsearchRepository<DeviceRegister, String>{

	List<DeviceRegister> findAllByuserId(String userId);
	
	DeviceRegister findBydeviceMacAddress(String deviceMacAddress);
	
	DeviceRegister findByownerName(String ownerName);
	
}
