package com.root.iot.repository.elastic;

import java.util.List; 

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.model.IOTDevice;

@Repository
public interface IOTDeviceElasticRepository extends ElasticsearchRepository<IOTDevice, String> {
	
	IOTDevice findBydeviceMacAddress(String deviceMacAddress);
	
	IOTDevice findByuserId(String userId);
	
	List<IOTDevice> findAllByuserId(String userId);
}
