package com.root.iot.repository.elastic;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.audit.IOTDeviceAudit;

@Repository
public interface IOTDeviceAuditElasticRepository extends ElasticsearchRepository<IOTDeviceAudit, String>{

}
