package com.root.iot.repository.jpa;
 
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.model.DAOUser;

@Repository
public interface UserJpaRepository extends JpaRepository<DAOUser, String>{

	DAOUser findByUsername(String username);
	
}
