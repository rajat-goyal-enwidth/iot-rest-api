package com.root.iot.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.root.iot.model.IOTDevice;

@Repository
public interface IOTDeviceJpaRepository extends JpaRepository<IOTDevice, String> {
	
	IOTDevice findBydeviceMacAddress(String deviceMacAddress);
	
	IOTDevice findByuserId(String userId);
	
	List<IOTDevice> findAllByuserId(String userId);
}
