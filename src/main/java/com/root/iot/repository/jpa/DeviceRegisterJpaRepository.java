package com.root.iot.repository.jpa;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.root.iot.model.DeviceRegister;

@Repository
public interface DeviceRegisterJpaRepository extends JpaRepository<DeviceRegister, String> {

	List<DeviceRegister> findAllByuserId(String userId);

	@Query("select d from DeviceRegister d where d.deviceMacAddress = ?1")
	DeviceRegister findBydeviceMacAddress(String deviceMacAddress);

	DeviceRegister findByownerName(String ownerName);
}
