package com.root.iot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.root.iot.mqtt.gateway.MqttGateway;
import com.root.iot.payload.service.PayloadType1Service;
import com.root.iot.payload.service.PayloadType2Service;
import com.root.iot.payload.service.PayloadType3Service;

@RestController
@RequestMapping("/mqtt/send")
public class MqttRestController {

	
	@Autowired
	private MqttGateway mqttGateway;

	@Autowired
	PayloadType1Service payloadService1;
	
	@Autowired
	PayloadType2Service payloadService2;
	
	@Autowired
	PayloadType3Service payloadService3;
	
		
	
	@PostMapping(value = "/sendMessage")
	public String sendMessage(@RequestParam String topic, @RequestParam String message) {
		mqttGateway.sendToMqtt(topic, message);
		return topic + " -> send message : " + message;
	}

	
	
	//PayLoad CASE 1 ; REST API	
	//In case 1 :- we will check whether the macAddress is registered or not 
	//if MacAddress is registered we will send payload :- {"type":1, "s":"s", "key":"ASHFHF$%$HF$$$EY"}
	//if MacAddress is not registered we will send payload :- {"type":1, "s":"f"}
	
	@PostMapping(value = "/sendAcknowledgementType1")
	public ResponseEntity<String> sendOnIotDeviceType1(@RequestParam String topic) throws JsonProcessingException
	{
		String message = payloadService1.deviceAcknowledgementType1(topic);
		
		return new ResponseEntity<String>(message,HttpStatus.OK);
	}
		
	
	
	//PayLoad CASE 2 In the case 2 : Input parameter will be {"type":2} 
	//when it reaches to the device then device will call re register.	
	//-> input topic will be :- MacAddress
	//-> publish message will be :- type : 2
	
	@PostMapping(value = "/sendAcknowledgementType2")
	public ResponseEntity<String> sendOnIotDeviceType2(@RequestParam String topic) {
	
		String message = payloadService2.deviceAcknowledgementType2(topic);
		
		return new ResponseEntity<String>(message,HttpStatus.OK);
	}
	
		
		
	@PostMapping(value = "/sendAcknowledgementType3")
	public ResponseEntity<String> sendOnIotDeviceType3(@RequestParam String topic, @RequestParam String command) {
	
		String message = payloadService3.deviceAcknowledgementType3(topic,command);
		
		return new ResponseEntity<String>(message,HttpStatus.OK);
		
	}
	
	
}	
	
	
	
	
	
	