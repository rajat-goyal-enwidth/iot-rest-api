package com.root.iot.controller;

import java.util.List; 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.root.iot.model.DeviceRegister;
import com.root.iot.model.IOTDevice;
import com.root.iot.service.jpa.DeviceRegisterJpaService;
import com.root.iot.service.jpa.IOTDeviceJpaService;

@RestController
@RequestMapping("/iot/user")
public class IOTRestController {

	@Autowired
	DeviceRegisterJpaService deviceRegisterService;
	
	@Autowired
	IOTDeviceJpaService iotDeviceService;

	
	@GetMapping("/welcome")
	public String welcomeUser()
	{		
		return "Hi, welcome to IOT";
	}
	 
	
	
	//api to add IOT device	
	@PostMapping("/addIotDevice")
	public ResponseEntity<IOTDevice> addIOTDevice(@RequestBody IOTDevice iotDevice)
	{
		IOTDevice iotdevice1 = iotDeviceService.saveIOTDevice(iotDevice);
		return new ResponseEntity<IOTDevice>(iotdevice1,HttpStatus.OK);
	}
	
	
	//api to get deviceID by using MAC_ADDRESS
	
	@GetMapping("/getDeviceidByMacAddress/{macAddress}")
	public ResponseEntity<String> findDeviceByMacAddress(@PathVariable String macAddress)
	{
		String deviceId = iotDeviceService.findDeviceID_ByMacAddress(macAddress);
		return new ResponseEntity<String>(deviceId,HttpStatus.OK);
	}
	
	
	
	//api to get all the added IOT Devices by particular user
	
	@GetMapping("/getAllIotDevicesAddedByUser/{userId}")
	public ResponseEntity<List<IOTDevice>> getAllIOTDevicesAddedByUser(@PathVariable String userId)
	{
		List<IOTDevice> devices = iotDeviceService.findAllIotDevicesAddedByUser(userId);
		return new ResponseEntity<List<IOTDevice>>(devices,HttpStatus.OK);
	}
	
	
		
	//api to delete IOTDevice
	
	@DeleteMapping("/deleteIotDevice/{id}")
	public ResponseEntity<IOTDevice> deleteIOTDevice(@PathVariable String id)
	{
		IOTDevice iotdevice1 = iotDeviceService.deleteIOTDevice(id);
		return new ResponseEntity<IOTDevice>(iotdevice1,HttpStatus.OK);
	}
	
	
	//api to register device 
	//user needs to provide ownerName, ownerPhoneNo, and macAddress
	//note:- all the details must be send in form format
	
	@PostMapping("/registerDevice")
	public ResponseEntity<DeviceRegister> addIOTDevice(@RequestParam("ownerName") String ownername, 
		@RequestParam("ownerPhoneNo") String ownerphoneno, @RequestParam("macAddress") String macaddress)
	{
		DeviceRegister deviceRegister = deviceRegisterService.saveDeviceRegister(ownername,ownerphoneno,macaddress);
		return new ResponseEntity<DeviceRegister>(deviceRegister,HttpStatus.OK);
	}
	
	
	//api to find registered device details by using mac address
	@GetMapping("/findRegisteredDeviceByMacAddress/{deviceMacAddress}")
	public ResponseEntity<DeviceRegister> findRegisterDeviceByDeviceId(@PathVariable String deviceMacAddress)
	{
		DeviceRegister deviceRegister = deviceRegisterService.findRegisteredDeviceByMacAddress(deviceMacAddress);
		return new ResponseEntity<DeviceRegister>(deviceRegister,HttpStatus.OK);
	}
	
	
	//api to find the regisered device by owner name
	//user need to provide ownername with which device has been registered
	
	@GetMapping("/findRegisteredDeviceByOwnerName/{ownerName}")
	public ResponseEntity<DeviceRegister> findRegisterDeviceByOwner(@PathVariable String ownerName)
	{
		DeviceRegister device = deviceRegisterService.findRegisteredDeviceByOwnerName(ownerName);
		return new ResponseEntity<DeviceRegister>(device,HttpStatus.OK);
	}

	
	//api to find all the registered device by particular user
	//user needs to provide its's userId
	
	@GetMapping("/findallRegisteredDevicesByUserId/{userId}")
	public ResponseEntity<List<DeviceRegister>> findDevice(@PathVariable String userId)
	{
		List<DeviceRegister> devices = deviceRegisterService.findAllRegisteredDeviceByUserID(userId);
		return new ResponseEntity<List<DeviceRegister>>(devices,HttpStatus.OK);
	}
	

	//api to update owner name and owner phone 
	//user needs to provide new owner name , new owner phone no, and registered mac address
	
	@PutMapping("/updateOwnerNameOwnerPhoneNo")
	public ResponseEntity<DeviceRegister> updateOwnerNameAndPhoneNo(@RequestParam("newownername") 
		String newName, @RequestParam("newownerphoneno") String newphoneno, 
			@RequestParam("registeredMacAddress") String oldmacaddress)
	{
		DeviceRegister device = deviceRegisterService.updateOwnerNameAndOwnerPhoneNo(newName, newphoneno, oldmacaddress);
		return new ResponseEntity<DeviceRegister>(device,HttpStatus.OK);
	}
	
	
}
