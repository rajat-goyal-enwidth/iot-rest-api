package com.root.iot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.root.iot.model.DAOUser;
import com.root.iot.service.jpa.AdminJpaService;
import com.root.iot.service.jpa.IOTDeviceJpaService;

@RestController
@RequestMapping("/iot/admin")
public class AdminController {
	
	@Autowired
	AdminJpaService adminService;
	
	@Autowired
	IOTDeviceJpaService iotDeviceService;
	
	
	@GetMapping("/")
	public String welcomeAdmin() {
		return "Hello Admin";
	}

	
	@GetMapping("/finduser/{username}")
	public ResponseEntity<DAOUser> findUser(@PathVariable String username) {
		
		DAOUser user = adminService.findUser(username);
		return new ResponseEntity<DAOUser>(user,HttpStatus.OK);
	}

	@DeleteMapping("/deleteuser/{username}")
	public ResponseEntity<DAOUser> deleteUser(@PathVariable String username)
	{
		DAOUser user = adminService.deleteUser(username);
		return new ResponseEntity<DAOUser>(user,HttpStatus.OK);
	}
	

}
