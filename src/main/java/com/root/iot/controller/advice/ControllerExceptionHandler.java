package com.root.iot.controller.advice;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.root.iot.exception.DeviceRegisterException;
import com.root.iot.exception.IOTDeviceException;
import com.root.iot.exception.IOTDeviceNotFoundByMacAddress;
import com.root.iot.exception.IOTDeviceNotFoundByUserId;
import com.root.iot.exception.RegisteredDeviceNotFoundByMacAddress;
import com.root.iot.exception.RegisteredDeviceNotFoundByOwnerName;
import com.root.iot.exception.RegisteredDeviceNotFoundByUserId;

@ControllerAdvice
public class ControllerExceptionHandler {

	
	@ExceptionHandler(IOTDeviceException.class)
	public ResponseEntity<String> MacAddressAlreadyExist(IOTDeviceException iotException)
	{
		return new ResponseEntity<String>(iotException.getMessage(), HttpStatus.CONFLICT);
	}
	
	
	@ExceptionHandler(IOTDeviceNotFoundByMacAddress.class)
	public ResponseEntity<String> handleIOTDeviceNotFoundExceptionByMacAddress(IOTDeviceNotFoundByMacAddress exception)
	{
		return new ResponseEntity<String>(exception.getMessage(),HttpStatus.NOT_FOUND);
	}

	
	@ExceptionHandler(IOTDeviceNotFoundByUserId.class)
	public ResponseEntity<String> handleIOTDeviceNotFoundExceptionByUserId(IOTDeviceNotFoundByUserId exception)
	{
		return new ResponseEntity<String>(exception.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	
	
	@ExceptionHandler(DeviceRegisterException.class)
	public ResponseEntity<String> handleRegisteredDeviceException(DeviceRegisterException deviceException)
	{
		return new ResponseEntity<String>(deviceException.getMessage(),HttpStatus.CONFLICT);
	}
	
	
	@ExceptionHandler(RegisteredDeviceNotFoundByMacAddress.class)
	public ResponseEntity<String> handleRegisteredDeviceNotFoundExceptionByDeviceId(RegisteredDeviceNotFoundByMacAddress exception)
	{
		return new ResponseEntity<String>(exception.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(RegisteredDeviceNotFoundByOwnerName.class)
	public ResponseEntity<String> handleRegisteredDeviceNotFoundExceptionByOwnerName(RegisteredDeviceNotFoundByOwnerName exception)
	{
		return new ResponseEntity<String>(exception.getMessage(),HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(RegisteredDeviceNotFoundByUserId.class)
	public ResponseEntity<String> handleRegisteredDeviceNotFoundExceptionByUserId(RegisteredDeviceNotFoundByUserId exception)
	{
		return new ResponseEntity<String>(exception.getMessage(),HttpStatus.NOT_FOUND);
	}
}
